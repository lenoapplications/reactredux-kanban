// @flow


//User reducer
export type UserState={

    serverResponse:{
        signupResponse:string;
        signinResponse:string;
    };

    userInfo:{
        logged:boolean;
        username:string;
        initialized:{
            status:boolean;
            userInfo:Object;
        }
    };
    users:{
        allUsers:Array<Object>;
    };
}

//Page controller
export type PageController={
    loginPage:{
        showForm:{
            signupActive:boolean
        }
    },
}
export type TagsState={
    recommendedTags:{
        list:Array<Object>
    },
    possibleTags:{
        list:Array<Object>
    }
}
export type TasksState={
    tasks:{
        allTasks:Object;
    };

}

export type Store = {
    user:UserState,
    pageController:PageController,
    tags:TagsState,
    tasks:TasksState,
}