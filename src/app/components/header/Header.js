// @flow
//Core
import React,{Component} from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
//StyleHandler
import {gridDisplay} from '../../utils/styleHandler/GeneralStyle';
import {maxWidth860,minWidth861} from "../../utils/styleHandler/ResponsiveHandler"
import {animationHeader} from "../../utils/styleHandler/Animations";
//Type
import type {Store} from "../../types/ReducersType";
import type {Div} from "../../types/GeneralTypes";


const MainDiv=styled.div
    `
    display:flex;
    align-self:center;
    overflow:hidden;
    border-radius:20%;
    ${minWidth861(`
        height:10%;
        width:100%;
    `)}

       
    ${maxWidth860(`
    
    
    `)}
    
    `;
const Kanban=styled.div
    `
    padding:1%;
    display: flex;
    height: 100%;
    overflow:visible;
    ${minWidth861(`
        
    `)}
    
    `

const H2=styled.h2
    `
    ${animationHeader()}
    font-weight: bold;
    color:gray;
    font-family:'NTR', sans-serif;
    letter-spacing: 10px;
    animation-name: animationHeader;
    animation-duration: 2.5s;
    font-size:4vmin;
    margin-top:1%;
    
    `


class Header extends Component<Div>{

    constructor(props){
        super(props);
    }

    render(){
        return(
            <MainDiv>
                <Kanban>
                    <H2>Kanban</H2>
                </Kanban>

            </MainDiv>
        );
    }
}



function mapStateToProps(store : Store){
    return{
    }
}
function mapDispatchToProps(dispatch : Function){

    return{
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(Header);