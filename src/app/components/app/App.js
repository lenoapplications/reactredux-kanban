// @flow
//Core
import React,{Component} from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';
import {BrowserRouter, Switch} from "react-router-dom";
import {Route} from "react-router";
import {bindActionCreators} from 'redux';
//Components
import Header from "../header/Header";
import Login from "../user/Login";
import Router from "./Router";
import Kanban from "../kanban/Kanban";
//StyleHandler
import {gridDisplay} from '../../utils/styleHandler/GeneralStyle';
import {maxWidth860,minWidth861} from "../../utils/styleHandler/ResponsiveHandler"
// Types
import type {Store} from "../../types/ReducersType";
import type {Div} from "../../types/GeneralTypes";


//Styled components
const MainDiv=styled.div
    `
    display:flex;
    flex-direction:column;
    height:100vh;
    margin-left:0.5%;
    margin-top:0.5%;
    ${minWidth861(
        `
        
        `
    )}
    
    ${maxWidth860(
        `
        `
    )}
    `
const Content=styled.div
    `
    height:90%;
    width:100%;
   
    `


class App extends Component<Div>{

    constructor(props){
        super(props)
    }

    render(){
        return(
            <MainDiv>
                <Header/>
                <Content>
                    <BrowserRouter>
                        <Switch>
                            <Route path="/" exact component={Router}/>
                            <Route path="/authorization" exact component={Login}/>
                            <Route path="/kanban" exact component={Kanban}/>
                        </Switch>
                    </BrowserRouter>
                </Content>
            </MainDiv>
        )

    }
}


function mapStateToProps(store : Store){
    return{
    }
}
function mapDispatchToProps(dispatch : Function){
    return{
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(App);