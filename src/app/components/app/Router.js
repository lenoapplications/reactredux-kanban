//@flow
//Core
import React,{Component} from 'react';
import { connect } from 'react-redux';
// Types
import type {Div} from "../../types/GeneralTypes";
import type {Store} from "../../types/ReducersType";



class Router extends Component<Div>{

    constructor(props){
        super(props);

        if (!props.userLogged){
            props.history.push("/authorization");
        }else{
            props.history.push("/kanban");
        }
    }
    render(){
        return (
            <div></div>
        )
    }
}



function mapStateToProps(store : Store){
    return{
        userLogged:store.user.userInfo.logged
    }
}
function mapDispatchToProps(dispatch : Function){
    return{
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(Router);