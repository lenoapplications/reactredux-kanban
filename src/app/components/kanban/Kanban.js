//@flow

//Core
import React,{Component} from 'react';
import styled,{keyframes} from 'styled-components';
import {bounce} from 'react-animations'
import { connect } from 'react-redux';
//Components
import Menu from "./parts/menu/Menu";
import Board from './parts/board/Board';
//Functions
import {fetchInitData} from "../../actions/kanbanAction/init/InitFunctions";
//StyleHandler
import {gridDisplay} from '../../utils/styleHandler/GeneralStyle';
import {maxWidth860,minWidth861} from "../../utils/styleHandler/ResponsiveHandler"
//Types
import type {Div} from "../../types/GeneralTypes";
import type {Store} from "../../types/ReducersType";




//Styled components

const MainDiv=styled.div
    `
    width:100%;
    height:100%;
    background-color:white;
    display:grid;
    ${minWidth861(`
    
        ${gridDisplay("0.5fr 10fr","auto")}
        grid-template-areas:"Menu Kanban "
        
    `)}
    
    ${maxWidth860(`
    
    `)}
    
    `;




class Kanban extends Component<Div>{

    constructor(props){
        super(props)

        if (!props.userLogged){
            props.history.push("/authorization");
        }
    }

    componentDidMount(){
        this.props.fetchInitData()
    }


    render(){
        const{initialized:{status}}=this.props;

        if (status === false){
            return (<div>Loading</div>)
        }else{
            return (
                <MainDiv>
                    <Menu/>
                    <Board/>
                </MainDiv>
             )
        }
    }
}



function mapStateToProps(store : Store){
    return{
        userLogged:store.user.userInfo.logged,
        initialized:store.user.userInfo.initialized
    }
}
function mapDispatchToProps(dispatch : Function){
    return{
        fetchInitData:()=>{dispatch(fetchInitData(dispatch))}
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(Kanban);