//@flow

//Core
import React,{Component} from 'react';
import styled,{keyframes} from 'styled-components';
import { connect } from 'react-redux';
//Components
import Search from './parts/search/Search';
import CreateTask from './parts/createTask/CreateTask';
//Functions
import {clickSearch,clickCreateTask} from "../../../../actions/kanbanAction/menu/MenuFunctions";
//StyleHandler
import {gridDisplay} from '../../../../utils/styleHandler/GeneralStyle';
import {maxWidth860,minWidth861} from "../../../../utils/styleHandler/ResponsiveHandler"
//Types
import type {Div} from "../../../../types/GeneralTypes";
import type {Store} from "../../../../types/ReducersType";


//Styled components


const MainDiv=styled.div
    `
    grid-area:Menu;
    display:flex;
    flex-direction:column;
    background-color:purple;
    width:60px;
    ${minWidth861(`
        
    `)}
    
    ${maxWidth860(`
    
    `)}
    
    `
const ItemDiv=styled.div
    `
    display:flex;
    background-color:red;
    margin-top:1vmin;
    height:7%;
    overflow:visible;
    `;
const Images=styled.img
    `
    position:relative;
    margin-left:5%;
    margin-top:5%;
    background-color:white;
    width:90%;
    height:80%;
    `


class Menu extends Component<Div>{

    constructor(props){
        super(props);
        this.state={
            searchSignal:"No init",
            createTaskSignal:"No init",
        }
    }

    render(){
        return(
            <MainDiv>
                <ItemDiv>
                    <Images onClick={clickSearch.bind(this)} src="../../../../../resources/icons/search.png"/>
                    <Search searchActive={this.state.searchSignal}/>
                </ItemDiv>
                <ItemDiv>
                    <Images onClick={clickCreateTask.bind(this)} src="../../../../../resources/icons/add.png"/>
                    <CreateTask createTaskActive={this.state.createTaskSignal}/>
                </ItemDiv>
            </MainDiv>
        );
    }
}

function mapStateToProps(store : Store){
    return{
        userLogged:store.user.userInfo.logged,
    }
}
function mapDispatchToProps(dispatch : Function){
    return{
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(Menu);