//@flow
//Core
import React,{Component} from 'react';
import styled,{keyframes} from 'styled-components';
import {connect} from "react-redux";
import {Redirect} from "react-router";
//StyleHandler
import {gridDisplay} from '../../../../../../utils/styleHandler/GeneralStyle';
import {maxWidth860,minWidth861} from "../../../../../../utils/styleHandler/ResponsiveHandler"
import {animationMenuShow,animationMenuHide} from "../../../../../../utils/styleHandler/Animations";
//Types
import type {Div} from "../../../../../../types/GeneralTypes";

//Styled components
const MainDiv=styled.div
    `
    display:flex;
    flex-wrap:wrap;
    justify-content:flex-start;
    width:99%;
    margin-bottom:0.9%;
    `;
const TagMark=styled.p
    `
    font-style:italic;
    font-weight:bold;
    margin:0;
    color:rgba(78, 65, 133, 0.84); 
    font-size:1.6vmin;
    letter-spacing:1px;
    `

const Tag=styled.p
    `
    font-style:italic;
    font-weight:bold;
    margin:0;
    font-size:1.6vmin;
    `
const TagInput=styled.input
    `
    border:none !important;
    width:40%;
    color:gray;
    font-size:1.5vmin;
    font-weight:bold;
    height:auto !important;
    margin:0 !important;
    padding:0 !important;
    `


function addNewTagIfActive(addInputSignal: string,
                           possibleTags: Array<string>,
                           changeStateForInputTag : Function,
                           onKeyPressSaveTag : Function,
                           onKeyUpSearchWord : Function){

    if (addInputSignal === "Active"){
        return <TagInput autoFocus type="text"
                         onKeyUp={(event)=>{onKeyUpSearchWord(event,possibleTags)}}
                         onKeyPress={onKeyPressSaveTag}
                         onBlur={changeStateForInputTag}/>
    }
}


export default (state: Object,
                props: Object,
                changeStateForInputTag : Function,
                onKeyPressSaveTag : Function,
                onTagPressRemove : Function,
                onKeyUpSearchWord : Function) : Div=>{
    return(
        <MainDiv onClick={(state.addInputTagSignal === "No active")?changeStateForInputTag:()=>{}}>
            <TagMark>Tags:</TagMark>
            {state.tagList.map((tag,index)=>{
                return (<Tag key={index} onClick={()=>{onTagPressRemove(index)}}>{tag}</Tag>)
            })}

            {addNewTagIfActive(state.addInputTagSignal,
                               props.possibleTags,
                               changeStateForInputTag,
                               onKeyPressSaveTag,
                               onKeyUpSearchWord)}

        </MainDiv>
    );
}

