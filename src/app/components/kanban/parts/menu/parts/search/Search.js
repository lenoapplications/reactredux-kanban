//@flow
//Core
import React,{Component} from 'react';
import styled,{keyframes} from 'styled-components';
import {connect} from "react-redux";
import {Redirect} from "react-router";
//StyleHandler
import {gridDisplay} from '../../../../../../utils/styleHandler/GeneralStyle';
import {maxWidth860,minWidth861} from "../../../../../../utils/styleHandler/ResponsiveHandler"
import {animationMenuShow,animationMenuHide} from "../../../../../../utils/styleHandler/Animations";
//Components
import createTags from './TagCreate';
import RecommendedTags from './RecommendedTags';
//Functions
import {activateAddInputTag,onEnterKeyPressSaveTag,onTagPressRemove,onKeyUpSearchWord} from "../../../../../../actions/kanbanAction/menu/search/SearchFunctions";
//Types
import type {Div} from "../../../../../../types/GeneralTypes";
import type {Store} from "../../../../../../types/ReducersType";




//Styled components

function createMainDiv(){

    switch(this.state.searchSignal){
        case "Active":
            return styledMainDiv(
                "20%",
                "0.5%",
                "1px",
                `${animationMenuShow("20%","70%")}
                 animation-name:showSearch;
                 animation-duration:1s;`);

        case "No active":
            return styledMainDiv(
                "0%",
                "0%",
                "0px",
                `${animationMenuHide("20%","70%")}
                 animation-name:hideSearch;
                 animation-duration:0.5s;`);

        case "TagInput signal":
            return styledMainDiv("20%","0.5%","1px",``);

        default:
            return styledMainDiv("0%","0%","0px",``);

    }
}

function styledMainDiv(width,padding,border,animation){
    return styled.div`
    position:absolute;
    left:70px;
    width:${width}
    height:25%;
    ${animation}
    background-color:white;
    border-bottom:${border} solid rgba(83, 65, 137, 0.6);
    border-right:${border} solid rgba(83, 65, 137, 0.6);
    border-top:${border} solid rgba(83, 65, 137, 0.6);
    padding:${padding}
   ::-webkit-scrollbar { 
    display: none; }
    overflow:auto;
    border-radius:5%;
    `
}

const InputDiv=styled.div
    `
    display:flex;  
    flex-direction:row;
    justify-content:center;
    flex-wrap:wrap;
    height:100%;
    width:100%;
    `


const Input=styled.input
    `
    width:80%;
    margin:auto;
    height:15% !important;
    color:rgba(14, 14, 28, 0.84):
    font-weight:bold;
    `;

const SearchInput=styled.input
    `
    width:50%;
    font-size:2vmin;
    height:40px !important;
    text-align:center;
    background-color:rgba(35, 32, 167, 0.84) !important;
    color:white !important;
    `;


class Search extends Component<Div>{

    constructor(props){
        super(props);
        this.state={
            searchSignal:props.searchActive,
            addInputTagSignal:"No active",
            tagList:[],
            recommendedTags:[]
        }
    }
    componentWillReceiveProps(nextProps){
       this.setState((prevState)=>{
         return   {...prevState,searchSignal:nextProps.searchActive};
       })
    }

    render(){
        const MainDiv=createMainDiv.bind(this)();

        const Tags=createTags(this.state,
                              this.props,
                              activateAddInputTag.bind(this),
                              onEnterKeyPressSaveTag.bind(this),
                              onTagPressRemove.bind(this),
                              this.props.onKeyUpSearchWord.bind(this));

        return(
            <MainDiv>
                {Tags}
                <RecommendedTags/>
                <InputDiv>
                    <Input type="text"/>
                    <SearchInput type="Submit" value="SEARCH"/>
                </InputDiv>
            </MainDiv>
        )
    }


}
function mapStateToProps(store : Store){
    return{
        possibleTags:store.tags.possibleTags.list,
    }
}
function mapDispatchToProps(dispatch : Function){
    return{
        onKeyUpSearchWord:(event,tags)=>{dispatch(onKeyUpSearchWord(event,tags,dispatch))}
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(Search);