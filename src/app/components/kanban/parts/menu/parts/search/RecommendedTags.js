//@flow
//Core
import React,{Component} from 'react';
import styled,{keyframes} from 'styled-components';
import {connect} from "react-redux";
import {Redirect} from "react-router";
//Functions
//StyleHandler
import {gridDisplay} from '../../../../../../utils/styleHandler/GeneralStyle';
import {maxWidth860,minWidth861} from "../../../../../../utils/styleHandler/ResponsiveHandler"
import {animationMenuShow,animationMenuHide} from "../../../../../../utils/styleHandler/Animations";
//Types
import type {Div} from "../../../../../../types/GeneralTypes";
import type {Store} from "../../../../../../types/ReducersType";

//Styled components
const MainDiv=styled.div
    `
    display:flex;
    flex-wrap:wrap;
    flex-direction:column;
    justify-content:flex-start;
    width:99%;
    margin-bottom:0.9%;
    border-top:1px gray solid;
    `;
const Header=styled.span
    `
    color:#858385 ;
    align-self:flex-start;
    letter-spacing:2px;
    font-size:2vmin;
    `
const Tag=styled.p
    `
    font-style:italic;
    font-weight:bold;
    letter-spacing:1px;
    margin:0;
    margin-left:10px;
    font-size:1.5vmin;
    `


class RecommendedTags extends Component<Div>{
    constructor(props){
        super(props)
    }


    render(){
        return (
            <MainDiv>
                {this.props.tags.length < 1?
                    null:
                    <Header>Suggestions :</Header>

                }
                {this.props.tags.map((tag,index)=>{
                    return (<Tag key={index}>{tag.name}</Tag>)
                })}
            </MainDiv>
        )
    }
}


function mapStateToProps(store : Store){
    return{
        tags:store.tags.recommendedTags.list
    }
}
function mapDispatchToProps(dispatch : Function){
    return{
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(RecommendedTags);

