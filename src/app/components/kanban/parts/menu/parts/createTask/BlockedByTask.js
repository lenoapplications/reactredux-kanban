//@flow

//Core
import React,{Component} from 'react';
import styled,{keyframes} from 'styled-components';



//Styled components

const MainDiv=styled.div
    `
    display:flex;
    flex-direction:column;
    width:50%;
    `;
const Label= styled.label
    `
    color:rgba(78, 65, 133, 0.84); 
    letter-spacing:2px;
    `;
const Select=styled.select
    `
    color:rgba(14, 14, 28, 0.84) ;
    font-weight:bold;
    align-self:center;
    width:100%;
    `;




export default (allTasks : Array<Object>)=>{

    return(
        <MainDiv>
            <Label htmlFor="blockedByTask">Required tasks: </Label>
            <Select type="text" id="blockedByTask">
                {allTasks.map((tasks,index)=>{
                    return <option key={index} value={tasks.id}>{tasks.name}</option>
                })}
            </Select>
        </MainDiv>

    )
}