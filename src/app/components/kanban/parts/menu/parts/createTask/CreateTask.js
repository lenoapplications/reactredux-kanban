//@flow

//Core
import React,{Component} from 'react';
import styled,{keyframes} from 'styled-components';
import { connect } from 'react-redux';
//Functions
import {createNewTask} from "../../../../../../actions/kanbanAction/menu/createTask/CreateTaskFunctions";
import createAssignee from './Assignee';
import createBlockedBy from './BlockedByTask';
import createCheckBox from './TagsCheckBox';
import {createName,createDescr} from "./NameAndDescr";
import createStatus from './Status';
//StyleHandler
import {gridDisplay} from '../../../../../../utils/styleHandler/GeneralStyle';
import {maxWidth860,minWidth861} from "../../../../../../utils/styleHandler/ResponsiveHandler"
import {animationMenuShow,animationMenuHide} from "../../../../../../utils/styleHandler/Animations";
//Types
import type {Div} from "../../../../../../types/GeneralTypes";
import type {Store} from "../../../../../../types/ReducersType";




//Styled components
function createMainDiv(){
    switch(this.state.createTaskSignal){
        case "Active":
            return styledMainDiv(
                "30%",
                "0.5%",
                "1px",
                `${animationMenuShow("30%","75%")}
                 animation-name:showSearch;
                 animation-duration:1s;`);

        case "No active":
            return styledMainDiv(
                "0%",
                "0%",
                "0px",
                `${animationMenuHide("30%","75%")}
                 animation-name:hideSearch;
                 animation-duration:0.5s;`);

        case "TagInput signal":
            return styledMainDiv("30%","0.5%","1px",``);

        default:
            return styledMainDiv("0%","0%","0px",``);

    }
}

function styledMainDiv(width,padding,border,animation){
    return styled.div`
    display:flex;
    flex-direction:column;
    position:absolute;
    left:70px;
    width:${width}
    height:75%;
    ${animation}
    background-color:white;
    border-bottom:${border} solid rgba(83, 65, 137, 0.6);
    border-right:${border} solid rgba(83, 65, 137, 0.6);
    border-top:${border} solid rgba(83, 65, 137, 0.6);
    padding:${padding}
   ::-webkit-scrollbar { 
    display: none; }
    overflow:auto;
    border-radius:1%;
    `
}
const Header=styled.span
    `
    color:#858385 ;
    align-self:flex-start;
    letter-spacing:2px;
    font-size:2vmin;
    `
const FormDiv=styled.div
    `
    display:flex;
    flex-direction:column;
    width:99%;
    height:80%;
    `
const Form=styled.form
    `
    display:flex;
    flex-direction:row;
    flex-wrap:wrap;
    `;

const Submit=styled.input
    `
     background-color:rgba(35, 32, 167, 0.84) !important;
     color:white !important;
     font-weight:bold;
     align-self:center;
     width:60%;
    `;


class CreateTask extends Component<Div>{
    constructor(props){
        super(props);
        this.state={
            createTaskSignal:props.createTaskActive,
        }
    }

    componentWillReceiveProps(nextProps){
        this.setState((prevState)=>{
            return   {...prevState,createTaskSignal:nextProps.createTaskActive};
        })
    }

    render(){
        const MainDiv=createMainDiv.bind(this)();
        const Assignee=createAssignee(this.props.allUsers);
        const BlockedBy=createBlockedBy(this.props.allTasks);
        const Name=createName();
        const Descr=createDescr();
        const Status=createStatus();
        return(
            <MainDiv>
                <Header>Create new task: </Header>
                <FormDiv>
                    <Form onSubmit={this.props.createTask}>

                        {Name}
                        {Descr}
                        {Assignee}
                        {BlockedBy}
                        {this.props.allTags.map((tag,index)=>{
                            return createCheckBox(tag)
                        })}
                        {Status}
                        <Submit type="submit" value="Create task"/>
                    </Form>
                </FormDiv>
            </MainDiv>
        );
    }
}


function mapStateToProps(store : Store){
    return{
        allUsers:store.user.users.allUsers,
        allTags:store.tags.possibleTags.list,
        allTasks:store.tasks.tasks.allTasks,
    }
}
function mapDispatchToProps(dispatch : Function){
    return{
        createTask:(event)=>{dispatch(createNewTask(event,dispatch))}
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(CreateTask);