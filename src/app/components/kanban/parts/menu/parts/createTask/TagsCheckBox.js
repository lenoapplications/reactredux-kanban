//@flow

//Core
import React,{Component} from 'react';
import styled,{keyframes} from 'styled-components';



//Styled components


const MainDiv =styled.div
    `
    display:flex;
    width:30%;
    `;
const Label= styled.label
    `
    color:rgba(78, 65, 133, 0.84); 
    letter-spacing:2px;
    `;
const CheckBox=styled.input
    `
    margin-top:2%;
    margin-left:auto;
    margin-right:10%;
    `;




export default (tag : Object)=>{

    return(
            <MainDiv key={tag.id}>
                <Label htmlFor={tag.id}>{tag.name}</Label>
                <CheckBox type="checkbox" value={tag.id} id={tag.id}/>
            </MainDiv>
    );
}