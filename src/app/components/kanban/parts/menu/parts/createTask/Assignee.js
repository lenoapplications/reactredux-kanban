//@flow

//Core
import React,{Component} from 'react';
import styled,{keyframes} from 'styled-components';
import type {Div} from "../../../../../../types/GeneralTypes";


//Styled components
const MainDiv=styled.div
    `
    display:flex;
    flex-direction:column;
    width:50%;
    `;
const Label= styled.label
    `
    color:rgba(78, 65, 133, 0.84); 
    letter-spacing:2px;
    `;
const Select=styled.select
    `
    color:rgba(14, 14, 28, 0.84) ;
    font-weight:bold;
    align-self:center;
    width:100%;
    `;



export default (allUsers : Array<Object>) : Div=>{

    return(
        <MainDiv>
            <Label htmlFor="taskAssignee">Assignee: </Label>
            <Select type="text" id="taskAssignee">
                {allUsers.map((user,index)=>{
                    return <option key={index} value={user.id}>{user.name}</option>
                })}
            </Select>
        </MainDiv>
    );
}