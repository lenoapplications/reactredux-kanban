//@flow

//Core
import React,{Component} from 'react';
import styled,{keyframes} from 'styled-components';



//Styled components

const NameDiv=styled.div
    `
    display:flex;
    flex-direction:column;
    width:50%;
    `;
const Label= styled.label
    `
    color:rgba(78, 65, 133, 0.84); 
    letter-spacing:2px;
    `;
const NameInput=styled.input
    `
     color:rgba(14, 14, 28, 0.84) ;
     font-weight:bold;
     align-self:center;
     width:100%;
    `;
const DescrDiv=styled.div
    `
    display:flex;
    flex-direction:column;
    width:90%;
    `;
const DescrInput=styled.textarea
    `
    color:rgba(14, 14, 28, 0.84) ;
    font-weight:bold;
    width:100%;
    `;

export function createName (){
    return(
        <NameDiv>
            <Label htmlFor="taskName">Name: </Label>
            <NameInput type="text" id="taskName"/>
        </NameDiv>
    )
}

export function createDescr(){
    return(
        <DescrDiv>
            <Label htmlFor="taskDescr">Description: </Label>
            <DescrInput type="text" id="taskDescr"/>
        </DescrDiv>
    )
}