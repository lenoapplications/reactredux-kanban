//@flow

//Core
import React,{Component} from 'react';
import styled,{keyframes} from 'styled-components';

//Styled components
const MainDiv =styled.div
    `
    display:flex;
    flex-direction:column;
    width:50%;
    `;

const Label= styled.label
    `
    color:rgba(78, 65, 133, 0.84); 
    letter-spacing:2px;
    `;

const Select=styled.select
    `
    color:rgba(14, 14, 28, 0.84) ;
    font-weight:bold;
    width:100;
    `;


export default ()=>{
    return(
        <MainDiv>
            <Label htmlFor="taskStatus">Status: </Label>
            <Select type="text" id="taskStatus">
                <option value="TODO">TODO</option>
                <option value="IN_PROGRESS">IN PROGRESS</option>
                <option value="DONE">DONE</option>
            </Select>
        </MainDiv>
    );
}