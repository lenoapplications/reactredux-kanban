//@flow

//Core
import React,{Component} from 'react';
import styled,{keyframes} from 'styled-components';
import { connect } from 'react-redux';
import {DragDropContextProvider,DragDropContext} from 'react-dnd'
import HTML5Backend from'react-dnd-html5-backend';
//Components
import TaskHolder from './parts/TaskHolder';
//Functions
import {changeStatusTask} from "../../../../actions/kanbanAction/board/BoardFunctions";
//StyleHandler
import {gridDisplay} from '../../../../utils/styleHandler/GeneralStyle';
import {maxWidth860,minWidth861} from "../../../../utils/styleHandler/ResponsiveHandler"
//Types
import type {Div} from "../../../../types/GeneralTypes";
import type {Store} from "../../../../types/ReducersType";

//Styled components
const MainDiv=styled.div
    `
    grid-area:Kanban;
    margin:0 auto;
    margin-top:1%;
    padding:1%;
    border-radius:5%;
    background-color:ghostwhite;
    overflow-y:auto;
     ${minWidth861(`
    
        ${gridDisplay("1fr 1fr 1fr","auto")}
        grid-template-areas:"TODO INPROGRESS DONE "
        
    `)}
    width:70%;
    height:80%;
    `;





class Board extends Component<Div>{

    constructor(props){
        super(props);
        console.log(props);
    }
    render(){
        return(

            <MainDiv>
                <TaskHolder updateTask={this.props.changeStatusTask} stage={"TODO"} header={"Task to do"} gridArea={"TODO"}/>
                <TaskHolder updateTask={this.props.changeStatusTask} stage={"IN_PROGRESS"} header={"Task in progress"} gridArea={"INPROGRESS"}/>
                <TaskHolder updateTask={this.props.changeStatusTask} stage={"DONE"} header={"Task completed"} gridArea={"DONE"}/>
            </MainDiv>

        );
    }

}

function mapStateToProps(store : Store){
    return{

    }
}
function mapDispatchToProps(dispatch : Function){
    return{
        changeStatusTask : (task)=>{dispatch(changeStatusTask(dispatch,task))}
    }
}

export default DragDropContext(HTML5Backend)(connect(mapStateToProps,mapDispatchToProps)(Board));