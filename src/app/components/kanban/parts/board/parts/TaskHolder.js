//@flow

//Core
import React,{Component} from 'react';
import styled,{keyframes} from 'styled-components';
import {DropTarget} from 'react-dnd';
import { connect } from 'react-redux';
//Components
import Drag from './DragAndDrop';
//Functions
//StyleHandler
import {gridDisplay} from '../../../../../utils/styleHandler/GeneralStyle';
import {maxWidth860,minWidth861} from "../../../../../utils/styleHandler/ResponsiveHandler"
//Types
import type {Div} from "../../../../../types/GeneralTypes";
import type {Store} from "../../../../../types/ReducersType";
import {api} from "../../../../../utils/apiCaller/Axios";


//Styled components

const Header=styled.span
    `
    color:rgb(131, 126, 128); 
    font-family:'NTR', sans-serif;
    font-weight:bold;
    align-self:center;
    letter-spacing:2px;
    font-size:2vmin;
    `;






class TaskHolder extends Component<Div>{
    constructor(props){
        super(props);
        this.state={
            style:this.style(),
            tasks:this.organizeTasks(),
        };
    }
    style(){
        let border;
        if (this.props.stage === "DONE"){
            border="";
        }else{
            border="1px rgba(120, 108, 121, 0.42) solid ";
        }
        return {
            width:"95%",
            height:"100%",
            borderRight:border,
            padding:"1px",
            gridArea:this.props.gridArea,
            display:"flex",
            flexDirection:"column"
        }
    }
    organizeTasks(){
        const belongingTasks=this.props.tasks.filter((task)=>{
            return task.status === this.props.stage;
        });
        return belongingTasks;
    }

    componentWillReceiveProps(nextProps) {
        this.setState((prevState)=>{
            return {...prevState,tasks:this.organizeTasks()}
        })
    }

    render(){
        const {connectDragSource}=this.props;;
        return connectDragSource(
            <div style={this.state.style}>

                <Header>{this.props.header}</Header>

                {this.state.tasks.map((task)=>{
                    return (
                        <Drag type="Check" task={task}/>
                    );
                })}
            </div>

        )
    }
}


const target = {
    drop:(props,monitor,component)=> {
        const task = monitor.getItem();
        const url=`task/${task.id}`;

        task.status = props.stage;
        api.put(url,task).then((resp)=>{
        }).catch((err)=>{
            alert("Error sending the update task");
        });
        return {
            task:task,
        };
    }
    ,
}
const collect = (connect, monitor) => {
    return{
        connectDragSource: connect.dropTarget(),
        task:monitor.getItem(),
    }
};


function mapStateToProps(store : Store){
    return{
        tasks:store.tasks.tasks.allTasks
    }
}
function mapDispatchToProps(dispatch : Function){
    return{
    }
}

export default DropTarget("tasks",target,collect)(connect(mapStateToProps,mapDispatchToProps)(TaskHolder));