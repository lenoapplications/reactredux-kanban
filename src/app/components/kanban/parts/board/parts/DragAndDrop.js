//@flow

//Core
import React,{Component} from 'react';
import styled,{keyframes} from 'styled-components';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import {DragSource} from 'react-dnd';
//Components

//Functions

//StyleHandler
import {gridDisplay} from '../../../../../utils/styleHandler/GeneralStyle';
import {maxWidth860,minWidth861} from "../../../../../utils/styleHandler/ResponsiveHandler"
//Types
import type {Div} from "../../../../../types/GeneralTypes";
import type {Store} from "../../../../../types/ReducersType";



//Styled components

const Name=styled.span
    `
    font-size:15px;
    color:black;
    `;



class Card extends Component<Div> {
    constructor(props){
        super(props);
    }

    render() {

        const { isDragging, connectDragSource, text } = this.props;
        return connectDragSource(
            <div style={{height:"60px",
                        marginTop:"5px",
                        display:"flex",
                        justifyContent:"center",
                        width:"90%",
                        backgroundColor:"white",
                        padding:"10px"}}>

                <Name>{this.props.task.name}</Name>
            </div>
        );
    }
}


/**
 * Implements the drag source contract.
 */
const cardSource = {
    beginDrag(props) {
        return props.task;
        }
    ,
    endDrag(props,monitor){
    }
};

/**
 * Specifies the props to inject into your component.
 */
function collect(connect, monitor) {
    return {
        connectDragSource: connect.dragSource(),
        isDragging: monitor.isDragging()
    };
}

function mapStateToProps(store : Store){
    return{
    }
}
function mapDispatchToProps(dispatch : Function){
    return{

    }
}

export default DragSource("tasks", cardSource, collect)((connect(mapStateToProps,mapDispatchToProps)(Card)));
