// @flow
//Core
import React,{Component} from 'react';
import styled from 'styled-components';
import {keyframes} from 'styled-components'
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
//StyleHandler
import {gridDisplay} from '../../../utils/styleHandler/GeneralStyle';
import {maxWidth860,minWidth861} from "../../../utils/styleHandler/ResponsiveHandler"
import {animationResponseMessage} from "../../../utils/styleHandler/Animations";


//Styled components
const MainDiv=styled.div
    `
    grid-area:Form
    display:flex;
    flex-direction:column;
    align-items: center;
    height:90%;
    transition:width 2s,height 4s;
    ${minWidth861(

    )}
    
    ${maxWidth860(

    )}
    `
const GoBack=styled.button
    `
    border-color:white;
    text-align:left;
    width:40%;
    font-size:1.5vw;
    margin-top:auto;
    font-weight:bold;
     &:hover{
        color:purple;
        border-color:white;
    }
    `

const SignupForm=styled.form
    `
     display:flex;
     flex-direction: column;
     border:2px solid  rgba(16, 137, 23, 0.2);
     border-radius:2%;
    ${minWidth861(`
        height:350px;
        width:35%;
        padding:10px;
    `)}
    
    ${maxWidth860(`
    
    
    `)}
    
    `;
const InputText=styled.input
    `
    border-color:teal;
    letter-spacing:2px; 
    font-weight: bold;
    `;
const InputSubmit=styled.input
    `
    width:50%;
    align-self:center;  
    letter-spacing: 2px;
    margin-top: 7%;
    color:red;
    `;

const Label=styled.label
    `
     text-align: center;
     margin-top: 1%;
     letter-spacing: 5px;
     color:gray;
    `;

const DivStatus=styled.div
    `
    width:100%;
    height:30%;
    text-align: center;
    `;


function modifyRegisterStatus(props){
    const color=(props.signupResponse === "Account created")?"green":"red";
    const RegisterStatus=styled.p`
    font-size:2.0vmin;
    font-weight: bold;
    word-wrap: break-word;
    ${(color === 'red')?
        `
        color:red
        ${animationResponseMessage(["#79090e","red","#ff3005"])}
        animation-name: responseMessage ;
        animation-duration: 1s;
        animation-iteration-count: infinite;
        `
        :
        
        `
        color:green
        `
    }
        
    `;
    return RegisterStatus;

}

export default (props: Object)=>{
    const RegisterStatus=modifyRegisterStatus(props)
    return(
      <MainDiv>
          <GoBack onClick={()=>{props.switchForm(props.sigunpActive)}}>X</GoBack>

          <SignupForm onSubmit={(event)=>{props.signup(event)}}>
              <Label htmlFor="registerUsername">Username: </Label>
              <InputText type="text" id="registerUsername"/>

              <Label htmlFor="registerPassword">Password:</Label>
              <InputText type="password" id="registerPassword"/>

              <Label htmlFor="registerName">Name:</Label>
              <InputText type="text" id="registerName"/>

              <InputSubmit type="submit" value="Register"/>
          </SignupForm>

          <DivStatus>
              <RegisterStatus>{props.signupResponse}</RegisterStatus>
          </DivStatus>

      </MainDiv>

    );

}