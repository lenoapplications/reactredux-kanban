//Core
import React,{Component} from 'react';
import styled,{keyframes} from 'styled-components';
//StyleHandler
import {gridDisplay} from '../../../utils/styleHandler/GeneralStyle';
import {maxWidth860,minWidth861} from "../../../utils/styleHandler/ResponsiveHandler"
import {animationResponseMessage} from "../../../utils/styleHandler/Animations";
import {Redirect} from "react-router";




//Styled components

const MainDiv=styled.div
    `
    grid-area:Form
    display:flex;
    flex-direction:column;
    align-items: center;
    height:90%;
    ${minWidth861(
        
    )}
    
    ${maxWidth860(
        
    )}
    `

const LoginForm=styled.form
    `
     display:flex;
     flex-direction: column;
     margin-top:auto;
     border:2px solid  rgba(16, 0, 23, 0.09);
     border-radius:2%;
    ${minWidth861(`
        height:260px;
        width:35%;
        padding:10px;
    `)}
    
    ${maxWidth860(`
    
    
    `)}
    
    `
const InputText=styled.input
    `
     letter-spacing: 2px;
     font-weight: bold;
    `
const InputSubmit=styled.input
    `
      width:50%;
      align-self:center;
      letter-spacing: 2px;
      margin-top: 7%;
      color:black;
    `

const Label=styled.label
    `
     text-align: center;
     margin-top: 1%;
     letter-spacing: 5px;
     color:rgba(120, 67, 137, 0.9);
    `

const DivSignup=styled.div
    `
    width:100%;
    text-align:center;
    `
const Signup=styled.p
    `
    color:gray;
    `
const SignupButton=styled.button
    `
    border-color:white;
    padding:1px;
    &:hover{
        color:purple;
        border-color:white;
    }
    `

const DivStatus=styled.div
    `
    width:100%;
    height:30%;
    text-align: center;
    `

const LoginStatus=styled.p
    `
    ${animationResponseMessage(["#79090e","red","#ff3005"])}
    font-size:2.0vmin;
    font-weight: bold;
    word-wrap: break-word;
    animation-name: responseMessage ;
    animation-duration: 1s;
    animation-iteration-count: infinite;
    `



export default(props)=>{

    return(
      <MainDiv>
          <LoginForm onSubmit={props.login}>

              <Label htmlFor="loginUsername">Username: </Label>
              <InputText type="text" id="loginUsername"/>

              <Label htmlFor="loginPassword">Password:</Label>
              <InputText type="password" id="loginPassword"/>

              <InputSubmit type="submit" value="Login"/>
          </LoginForm>
          <DivSignup>
              <Signup>If you don't have account <SignupButton onClick={()=>{props.switchForm(props.sigunpActive)}}>CLICK HERE</SignupButton></Signup>
          </DivSignup>
          <DivStatus>
              <LoginStatus>{props.signinResponse}</LoginStatus>
          </DivStatus>

      </MainDiv>
    );
}

