// @flow
//Core
import React,{Component} from 'react';
import styled,{keyframes} from 'styled-components';
import { connect } from 'react-redux';
//Parts
import LoginForm from './parts/LoginForm'
import SignupForm from './parts/SignupForm'
//Functions
import {switchForm,login} from "../../actions/userAction/login/LoginFunctions";
import {signupNewUser} from "../../actions/userAction/signup/SignupFunctions";
//StyleHandler
import {gridDisplay} from '../../utils/styleHandler/GeneralStyle';
import {maxWidth860,minWidth861} from "../../utils/styleHandler/ResponsiveHandler"
//Types
import type {Store} from "../../types/ReducersType";
import type {Div} from "../../types/GeneralTypes";
import {Redirect} from "react-router";



//Styled components
const MainDiv=styled.div
    ` 
    ${minWidth861(
        `
         width:100%;
         height:100%;
         display:grid
         ${gridDisplay("1fr 2fr 1fr","1fr")}
         grid-template-areas:". Form ."
        
        `
    )}
    
    ${maxWidth860(
        `
          ${gridDisplay("1fr","1fr")}
        `
    )}
    `

class Login extends Component<Div>{

    constructor(props){
        super(props);
    }


    formToShow() : Object{
        if (this.props.userLogged){
            return <Redirect to="/kanban"/>
        }else{
            return (this.props.sigunpActive !== true ? LoginForm(this.props) : SignupForm(this.props))
        }
    }

    render(){
        const form : Object=this.formToShow()
        return(
            <MainDiv>
                {form}
            </MainDiv>
        );
    }
}



function mapStateToProps(store : Store){
    return{
        sigunpActive:store.pageController.loginPage.showForm.signupActive,
        signupResponse:store.user.serverResponse.signupResponse,
        signinResponse:store.user.serverResponse.signinResponse,
        userLogged:store.user.userInfo.logged,
    }
}
function mapDispatchToProps(dispatch : Function){

    return{
        switchForm:(bool)=>{dispatch(switchForm(bool,dispatch))},
        signup:(event)=>{dispatch(signupNewUser(event,dispatch))},
        login:(event)=>{dispatch(login(event,dispatch))},
    }
}


export default connect(mapStateToProps,mapDispatchToProps)(Login);
