// @flow
//Core
import {combineReducers} from 'redux'
//Reducers
import user from './user/UserState';
import pageController from './pageController/PageController';
import tags from './tags/TagsState';
import tasks from './tasks/TasksState';
// Types
import type {Store} from "../types/ReducersType";


const store : Store =combineReducers({
    user,
    pageController,
    tags,
    tasks,
});


export default store;
