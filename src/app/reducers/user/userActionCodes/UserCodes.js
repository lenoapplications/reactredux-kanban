export const userActionCodes={

    serverResponse:{
        signupResponse:"SIGNUP RESPONSE",
        signinResponse:"SIGNIN RESPONSE",
    },
    userInfo:{
        initialized:"INITIALIZED DATA"
    }
}