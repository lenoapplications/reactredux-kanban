// @flow
import {userActionCodes} from './userActionCodes/UserCodes';
import type {Action} from "../../types/ActionType";
import type{UserState} from "../../types/ReducersType";


const initialState : UserState={
    serverResponse:{
        signupResponse:"",
        signinResponse:"",
    },

    userInfo:{
        logged:checkToken(),
        username:"",
        initialized:{
            status:false,
            userInfo:{},
        },
    },
    users:{
        allUsers:[],
    }
}
function checkToken(): boolean{
    return localStorage.getItem("access_token") !== null;
}

function signupResponse(state: UserState,action : Action){
    return {...state,serverResponse:{...state.serverResponse,signupResponse : action.payload}}
}
function signinResponse(state: UserState,action : Action){
    return {...state,serverResponse:{...state.serverResponse, signinResponse : action.payload.response}
           ,userInfo:{...state.userInfo,logged : action.payload.logged}}
}
function initializedData(state : UserState, action : Action){
    return {...state,userInfo:{
            ...state.userInfo,initialized:{
                ...state.userInfo.initialized, status: action.payload.status,
                                               userInfo : action.payload.me,
            }
        },users:{...state.users,allUsers:action.payload.users}
    }
}

export default function(state : UserState=initialState,action : Action){
    switch(action.type) {

        case userActionCodes.serverResponse.signupResponse:
            return signupResponse(state,action);

        case userActionCodes.serverResponse.signinResponse:
            return signinResponse(state,action)

        case userActionCodes.userInfo.initialized:
            return initializedData(state,action);

        default:
            return state;
    }
}


