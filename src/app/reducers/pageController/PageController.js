// @flow
import type {Action} from "../../types/ActionType";
import type{PageController, UserState} from "../../types/ReducersType";
import {pageActionCodes} from "./pageActionCodes/PageFlags";


const initialState:PageController={
    loginPage:{
        showForm:{
            signupActive:false,
        }
    },
}
//
function signupClicked(state : PageController,action : Action){
    return{...state,loginPage:{
            ...state.loginPage,showForm:{
                ...state.loginPage.showForm,signupActive:action.payload,
            }
        }
    }
}


export default function(state : PageController=initialState,action : Action){

    switch(action.type) {
        case pageActionCodes.loginPage.showForm.signupActive:
            return signupClicked(state, action);

        default:
            return state;
    }
}
