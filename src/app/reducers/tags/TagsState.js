//@flow
import {tagsActionCodes} from "./tagsActionCodes/TagsCodes";
import type {Action} from "../../types/ActionType";
import type {TagsState} from "../../types/ReducersType";

const initialState : TagsState={
    recommendedTags:{
        list:[],
    },
    possibleTags:{
        list:[],
    }
}


function addNewList(state : TagsState, action : Action): TagsState{
    return {...state,recommendedTags:{
            ...state.recommendedTags,list:action.payload
            }
    }
}
function initializedData(state : TagsState, action : Action) : TagsState{
    return {...state,possibleTags:{
            ...state.possibleTags,list:action.payload.tags,
        }
    }
}

export default function (state : TagsState=initialState, action : Action){

    switch(action.type){

        case tagsActionCodes.recommendedTags.addNewLit:
            return addNewList(state,action);
        case tagsActionCodes.initialized.possibleTags:
            return initializedData(state, action);

        default:
            return state;
    }
}

