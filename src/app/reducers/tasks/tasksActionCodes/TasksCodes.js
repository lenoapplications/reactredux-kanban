export const tasksActionCodes={
    tasks:{
        initialized:"INITIALIZED DATA",
        createdTask:"CREATE TASK",
        updateTask:"UPDATE TASK"
    }
}