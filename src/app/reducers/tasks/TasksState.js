import type {Action} from "../../types/ActionType";
import type {TasksState} from "../../types/ReducersType";
import {tasksActionCodes} from "./tasksActionCodes/TasksCodes";



const initialState : TasksState={
    tasks:{
        allTasks:{}
    }
}

function initializedData(state : TasksState, action : Action){
    return {...state,tasks:{
            ...state.tasks,allTasks:action.payload.tasks
        }
    }
}
function createdTask(state : TasksState, action : Action){
    const list=state.tasks.allTasks;
    list.push(action.payload);
    return {...state,tasks:{
            ...state.tasks,allTasks:list,
        }
    }
}


export default function(state : TasksState=initialState, action : Action) : TasksState{
    switch(action.type) {

        case tasksActionCodes.tasks.initialized:
            return initializedData(state,action);

        case tasksActionCodes.tasks.createdTask:
            return createdTask(state,action);

        default:
            return state;
    }
}