import axios from 'axios'
import env from '../../../env/Environment'


export const userApi=axios.create({
    baseURL:`${env.api.url}`,
});

export const api=axios.create({
    baseURL: `${env.api.url}`
})


export function setupAxios(){
    userSetup();
    apiSetup();
}

function userSetup() {

    userApi.interceptors.response.use((config) => {

        if (config.data.data.error !== undefined) {
            return Promise.reject(config);
        } else {
            return Promise.resolve(config);
        }

    }, (err) => {
        return Promise.reject(err);
    })
}

function apiSetup(){
    const token=localStorage.getItem("access_token");

    if (token){
        api.defaults.headers.common["Authorization"]="Bearer "+token;
    }
}