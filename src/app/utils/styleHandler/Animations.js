export function animationHeader(){
    return `
    @keyframes animationHeader {

        0% {
            letter-spacing: -20px;
            color:black;
        }
        50%
        {
            letter-spacing: 20px;
            color:gray;
        }
        100%{
            letter-spacing: 10px;
        }
    }`
}

export function animationResponseMessage(colors){
    return `
    @keyframes responseMessage{
        0%{
            color: ${colors[0]}
        }
        50%{
            color:${colors[1]};
        }
        100%{
            color:${colors[2]};
        }
    } 
    `
}

export function animationMenuShow(width, height){
    return `
    @keyframes showSearch{
        0%{
            width:1%;
            height:1%;
        }
        100%{
            height:${height}%;
            width:${width}%;
        }
    } 
    `
}
export function animationMenuHide(width, height){
    return `
    @keyframes hideSearch{
        0%{
            border:1px;
            padding:0.5%;
            height:${height}%;
            width:${width}%;
        }
        100%{
            border:0px;
            padding:0%;
            height:0%;
            width:0%;
        }
    }
    `
}



export default {animationHeader,animationResponseMessage}