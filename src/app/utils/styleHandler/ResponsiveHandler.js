

export function minWidth861(style){
    return`
    @media screen and (min-width: 861px){
        ${style}
    }
    
    `

}



export function maxWidth860(style){
    return`
    @media screen and (max-width: 860px){
        ${style}
    }
    `
}



export default {minWidth861,maxWidth860}