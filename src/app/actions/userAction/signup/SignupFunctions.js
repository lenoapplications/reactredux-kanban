// @flow

//Core
import {userApi} from "../../../utils/apiCaller/Axios";
import {signupResponse} from "./SignupActions";


export function signupNewUser(event:Object,dispatch: Function): Function{
    return (dispatch : Function)=>{
        event.preventDefault();

        const childNodes=event.target.childNodes;
        let response;

        if ( (response = validForm( childNodes ) ).length > 1){
            return dispatch(signupResponse(response))
        }else{
            const json=createJson( childNodes );
            return sendUserInfo(json,dispatch);
        }
    }
}
function createJson(childNodes:Array<Object>):Object{

    return{
        username:childNodes[1].value,
        password:childNodes[3].value,
        name:childNodes[5].value,
    }
}
function validForm(childNodes:Array<Object>):string{
    const username=childNodes[1];
    const password=childNodes[3];
    const name=childNodes[5];
    let response="";

    if (username.value.length < 5){
        response+=" --Username "
    }
    if (password.value.length < 5){
        response+=" --Password  "
    }
    if (name.value.length < 5){
        response+=" --Name "
    }
    return (response.length > 5)?`Minimum 5 chars. are required (${response})` : response
}

function accountCreated(dispatch : Function,resp : Object){
    localStorage.setItem("access_token",resp.data.data.token);
    return dispatch(signupResponse("Account created"))
}
function accountNotCreated(dispatch : Function){
    return dispatch(signupResponse("Account was not created"));
}
function sendUserInfo(json: Object,dispatch: Function){
    userApi.post("/signup",json)
        .then((resp)=>{accountCreated(dispatch,resp)})
        .catch((err)=>{accountNotCreated(dispatch)})
}