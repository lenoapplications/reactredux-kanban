// @flow
import {userActionCodes} from "../../../reducers/user/userActionCodes/UserCodes";
import type {Action} from "../../../types/ActionType";



export function signupResponse(response:string): Action{

    return {
        type:userActionCodes.serverResponse.signupResponse,
        payload:response,
    }
}