// @flow
import {userActionCodes} from "../../../reducers/user/userActionCodes/UserCodes";
import {pageActionCodes} from "../../../reducers/pageController/pageActionCodes/PageFlags";
import type {Action} from "../../../types/ActionType";


export function signupClicked(clicked: boolean):Action{
    return{
        type:pageActionCodes.loginPage.showForm.signupActive,
        payload:clicked,
    }
}
export function signinResponse(response : string,logged:boolean){
    return{
        type:userActionCodes.serverResponse.signinResponse,
        payload:{
            response,
            logged,
        }
    }
}


export default{signupClicked}