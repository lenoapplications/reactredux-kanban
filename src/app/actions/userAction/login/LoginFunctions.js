// @flow
//Core
import {userApi,api} from "../../../utils/apiCaller/Axios";
//Actions
import {signupClicked,signinResponse} from "./LoginActions";




export function switchForm(bool: boolean, dispatch: Function):Function{
    return (dispatch : Function) : Function=>{
        bool=!bool;
        return dispatch(signupClicked(bool));
    }
}
export function login(event: Object, dispatch: Function):Function{
    return (dispatch)=>{
        event.preventDefault();

        const childNodes=event.target.childNodes;
        let response="";

        if ( (response = validForm( childNodes ) ).length > 1){
            return dispatch(signinResponse(response,false))
        }else{
            const json=createJson(childNodes);
            return sendUserInfo(json,dispatch);
        }
    }
}

function validForm(childNodes:Array<Object>):string{
    const username=childNodes[1];
    const password=childNodes[3];
    let response="";

    if (username.value.length < 5){
        response+=" --Username "
    }
    if (password.value.length < 5){
        response+=" --Password  "
    }
    return (response.length > 5)?`Minimum 5 chars. are required (${response})` : response
}

function createJson(childNodes:Array<Object>):Object{

    return{
        username:childNodes[1].value,
        password:childNodes[3].value,
    }
}


function loginSuccess(dispatch: Function,resp : Object){
    const token=resp.data.data.token;
    localStorage.setItem("access_token",token);
    api.defaults.headers.common["Authorization"]="Bearer "+token;
    return dispatch(signinResponse("",true));
}
function loginNotSuccess(dispatch: Function){
    return dispatch(signinResponse("This username is not authorized !",false));
}
function sendUserInfo(json: Object,dispatch: Function){
    userApi.post("/signin",json)
        .then((resp)=>{loginSuccess(dispatch,resp)})
        .catch((err)=>{loginNotSuccess(dispatch)})
}

