//@flow
//Core
//Actions
import {tagsActionCodes} from "../../../../reducers/tags/tagsActionCodes/TagsCodes";
import type {Action} from "../../../../types/ActionType";

export function newRecommendedTags(tags : Array<Object>) : Action{
    return {
        type:tagsActionCodes.recommendedTags.addNewLit,
        payload:tags,
    }
}