//@flow
//Core
//Actions
import {newRecommendedTags} from "./SearchActions";

export function activateAddInputTag(){
    const inputTag=(this.state.addInputTagSignal === "No active")? "Active" : "No active";
    const searchDiv="TagInput signal";
    this.setState((prevState)=>{
        return {...prevState,addInputTagSignal:inputTag,searchSignal:searchDiv};
    });
}


export function onEnterKeyPressSaveTag(event:Object){
    if (event.key === 'Enter'){
        let value : string=event.target.value;

        if (value.length > 0){
            value=(value[0] === "#")?value : " #"+value;
            const list=this.state.tagList;

            list.push(value);
            this.setState((prevState)=>{
                return {...prevState,tagList:list,addInputTagSignal:"No active"}
            })
        }
    }
}
export function onTagPressRemove(index : number){
    const list=this.state.tagList;
    list.splice(index,1);
    this.setState((prevState)=>{
        return {...prevState,tagList:list}
    })
}


export function onKeyUpSearchWord(event : Object,tags: Array<Object>,dispatch : Function){

    return (dispatch : Function)=>{
        const value=event.target.value;
        if (value.length > 0) {
            const newTags = tags.filter((object) => object.name.startsWith(value))
            return dispatch(newRecommendedTags(newTags));
        }else{
            return dispatch(newRecommendedTags([]));
        }
    }
}