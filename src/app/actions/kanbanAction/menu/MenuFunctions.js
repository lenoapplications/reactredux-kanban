//@flow
//Core
//Actions


export function clickSearch(){
    let searchDiv;
    if (this.state.searchSignal === "No init"){
        searchDiv="Active";
    }else{
        searchDiv=(this.state.searchSignal === "No active")? "Active" : "No active";
    }

    if (this.state.createTaskSignal === "Active"){
        this.setState((prevState)=>{
            return {...prevState,createTaskSignal:"No active",searchSignal:searchDiv};
        })
    }else{
        this.setState((prevState)=>{
            return {...prevState,searchSignal:searchDiv};
        })
    }
}
export function clickCreateTask(){
    let createTaskDiv;
    if (this.state.createTaskSignal === "No init"){
        createTaskDiv="Active";
    }else{
        createTaskDiv=(this.state.createTaskSignal === "No active")? "Active" : "No active";
    }

    if (this.state.searchSignal === "Active"){
        this.setState((prevState)=>{
            return {...prevState,createTaskSignal:createTaskDiv,searchSignal:"No active"};
        })
    }else{
        this.setState((prevState)=>{
            return {...prevState,createTaskSignal:createTaskDiv};
        })
    }


}


