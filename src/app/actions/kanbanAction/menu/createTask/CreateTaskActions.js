//@flow

import type {Action} from "../../../../types/ActionType";
import {tasksActionCodes} from "../../../../reducers/tasks/tasksActionCodes/TasksCodes";

export function createTask(task : Object) : Action{
    return {
        type:tasksActionCodes.tasks.createdTask,
        payload:task,
    }
}