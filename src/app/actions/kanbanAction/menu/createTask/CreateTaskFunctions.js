//@flow
//Core
import {api, userApi} from "../../../../utils/apiCaller/Axios";

//Actions
import {createTask} from "./CreateTaskActions";

function getData(form:Object){
    const divs=form.childNodes;
    const name=divs[0].childNodes[1];
    const description=divs[1].childNodes[1];
    const assignee=divs[2].childNodes[1];
    const blockedBy=divs[3].childNodes[1];
    const checkBoxes=getCheckBoxes(divs);
    const status=divs[9].childNodes[1];

    return readyDataToSend(name,description,assignee,blockedBy,checkBoxes,status);
}

function readyDataToSend(name:Object,
                  descr:Object,
                  assignee:Object,
                  blocked:Object,
                  tags:Array<string>,
                  status:Object){


    const userAssignedTask=assignee.options[assignee.selectedIndex].value;
    const blockedByTask=parseInt(blocked.options[blocked.selectedIndex].value);
    const statusTask=status.options[status.selectedIndex].value;
    const tagsToNumber=tags.map((tag)=>{return parseInt(tag)})

    return{
        name:name.value,
        description:descr.value,
        status:statusTask,
        assignee:userAssignedTask,
        tags:tagsToNumber,
        blockedBy:[blockedByTask]
    }
}
function getCheckBoxes(divs:Object){
    const list=[];
    for(let i=4; i < 9; i++){
        const check=divs[i].childNodes[1];
        if (check.checked) {
            list.push(divs[i].childNodes[1].value)
        }
    }
    return list;
}

function sendTaskData(json: Object,dispatch: Function){
    api.post("/task",json)
        .then((resp)=>{dataSentSuccess(dispatch,resp)})
        .catch((err)=>{dataSentFailed(dispatch,err)})
}

function dataSentSuccess(dispatch: Function, resp: Object){
    return dispatch(createTask(resp.data.data))
}
function dataSentFailed(dispatch: Function, err: Object){
    console.log(err)
}

export function createNewTask(event : Object, dispatch : Function):Function{
    return(dispatch)=>{
        event.preventDefault();
        const form=event.target;
        const data=getData(form);
        return sendTaskData(data,dispatch);
    }
}