//@flow
//Core
import {api} from "../../../utils/apiCaller/Axios";
//Actions
import {updateTaskStatus} from "./BoardActions";


export function changeStatusTask(dispatch : Function,task : Object){
    const url=`task/${task.id}`;

    api.put(url,task).then((resp)=>{
    }).catch((err)=>{
        alert("Error sending the update task");
    })
}