import {tasksActionCodes} from "../../../reducers/tasks/tasksActionCodes/TasksCodes";
import type {Action} from "../../../types/ActionType";



export function updateTaskStatus(task : Object){
    return{
        type:tasksActionCodes.tasks.updateTask,
        payload:task,
    }
}