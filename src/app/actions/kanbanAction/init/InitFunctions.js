// @flow
//Core
import {api} from "../../../utils/apiCaller/Axios";
//Actions
import {initializeData} from "./InitActions";

function meResponse(dispatch : Function, resp : Object,initDataStorage : Object,) : Function {
    initDataStorage["me"]=resp.data.data;
    return checkRequestDone(dispatch,initDataStorage);
}
function tagsResponse(dispatch : Function, resp : Object,initDataStorage : Object,) : Function {
    initDataStorage["tags"]=resp.data.data;
    return checkRequestDone(dispatch,initDataStorage);
}
function allTasks(dispatch : Function, resp : Object,initDataStorage : Object,) : Function{
    initDataStorage["tasks"]=resp.data.data;
    return checkRequestDone(dispatch,initDataStorage);
}
function allUsers(dispatch : Function, resp : Object,initDataStorage : Object,) : Function{
    initDataStorage["users"]=resp.data.data;
    return checkRequestDone(dispatch,initDataStorage);
}
function checkRequestDone(dispatch : Function, initDataStorage : Object) : Function{
    const requestDone=Object.keys(initDataStorage).length;

    if (requestDone === 4){
        initDataStorage["status"]=true;
        return dispatch(initializeData(initDataStorage));
    }
    return ()=>{}
}

export function fetchInitData(dispatch : Function) : Function{
    return (dispatch)=>{
        const initializedData={}

        api.get("/me").then((resp) => {meResponse(dispatch,resp,initializedData)});
        api.get("/tags").then((resp) => {tagsResponse(dispatch,resp,initializedData) });
        api.get("/tasks").then((resp) => {allTasks(dispatch, resp, initializedData)});
        api.get("/users").then((resp)=> {allUsers(dispatch, resp , initializedData)})
    }
}