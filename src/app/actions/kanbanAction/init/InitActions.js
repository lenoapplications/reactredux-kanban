import {userActionCodes} from "../../../reducers/user/userActionCodes/UserCodes";
import type {Action} from "../../../types/ActionType";


export function initializeData(data: Object){
    return {
        type:userActionCodes.userInfo.initialized,
        payload:data,
    }
}