//Core
import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {applyMiddleware,createStore} from 'redux'
import thunk from 'redux-thunk'
import {setupAxios} from "./app/utils/apiCaller/Axios";
import registerServiceWorker from './registerServiceWorker';
import Reducer from './app/reducers/Store';
//Components
import App from "./app/components/app/App";
//CSS
import Indexcss from './index.css';
import type {Store} from "./app/types/ReducersType";
import normalizeCss from './lib/sceletonCss/Skeleton-2.0.4/css/normalize.css';
import sceletonCss from './lib/sceletonCss/Skeleton-2.0.4/css/skeleton.css';
import Login from "./app/components/user/Login";


const store : Store=createStore(Reducer,applyMiddleware(thunk));
setupAxios();

ReactDOM.render(
    <Provider store={store}>
        <App/>
    </Provider>
    , document.getElementById('root'));

registerServiceWorker();
